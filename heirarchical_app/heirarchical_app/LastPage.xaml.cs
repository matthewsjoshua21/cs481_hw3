﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace heirarchical_app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LastPage : ContentPage
    {
        public LastPage()
        {
            InitializeComponent();
        }
        async void OnMirrorClick(object sender, EventArgs e)
        {
            App.last_page = "MirrorBtn";
            await Navigation.PushAsync(new MirrorPage());
        }

        async void OnMindClick(object sender, EventArgs e)
        {
            App.last_page = "MindBtn";
            await Navigation.PushAsync(new MindPage());
        }

        async void OnLastClick(object sender, EventArgs e)
        {
            App.last_page = "LastBtn";
            await Navigation.PushAsync(new LastPage());
        }

        private void OnSuperCick(object sender, EventArgs e)
        {
            App.last_page = "SuperPop";
            Navigation.PopToRootAsync();
        }
    }

}