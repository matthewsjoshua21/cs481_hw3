﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace heirarchical_app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MindPage : ContentPage
    {
        public MindPage()
        {
            InitializeComponent();
            //Call function to launch youtube video on page init
            BrowserLaunchMode(this, EventArgs.Empty);

        }

        //Function to open uri
        public void BrowserLaunchMode(object sender, EventArgs e)
        {
            Launcher.OpenAsync("https://www.youtube.com/watch?v=2xp0P0gQDXw");
            //Device.OpenUri(new Uri("https://www.youtube.com/watch?v=2xp0P0gQDXw"));
        }
        async void OnMirrorClick(object sender, EventArgs e)
        {
            App.last_page = "MirrorBtn";
            await Navigation.PushAsync(new MirrorPage());
        }

        async void OnMindClick(object sender, EventArgs e)
        {
            App.last_page = "MindBtn";
            await Navigation.PushAsync(new MindPage());
        }
        async void CalcBtn_OnClicked(object sender, EventArgs e)
        {
            App.last_page = "CalcBtn";
            await Navigation.PushAsync(new Calculator());
        }
        async void OnLastClick(object sender, EventArgs e)
        {
            if (App.last_page == "MirrorBtn")
            {
                App.last_page = "LastBtn";
                await Navigation.PushAsync(new MirrorPage());
            }
            else if (App.last_page == "MindBtn")
            {
                App.last_page = "LastBtn";
                await Navigation.PushAsync(new MindPage());
            }
            else if (App.last_page == "SuperPop")
            {
                App.last_page = "LastBtn";
                await Navigation.PopToRootAsync();
            }
            else if (App.last_page == "LastBtn")
            {
                App.last_page = "LastBtn";
                await DisplayAlert("Alert!", "This is not inception", "Ok");
            }
            else if (App.last_page == "CalcBtn")
            {
                App.last_page = "CalcBtn";
                await Navigation.PushAsync(new Calculator());
            }
            else
            {
                await DisplayAlert("System Error", "No valid navigation selected", "Ok");
            }

            // await Navigation.PushAsync(new LastPage());
        }
        private void OnSuperCick(object sender, EventArgs e)
        {
            App.last_page = "SuperPop";
            Navigation.PopToRootAsync();
        }
        //Handle onAppearing and onDisappearing events. 
        private void OnAppearing(object sender, EventArgs e)
        {
            base.OnAppearing();
            UserDialogs.Instance.Toast("Opening MindPage...", TimeSpan.MinValue);
        }

        private void OnDisappearing(object sender, EventArgs e)
        {
            base.OnDisappearing();
            UserDialogs.Instance.Toast("Leaving MindPage...", TimeSpan.MinValue);
        }
    }

}