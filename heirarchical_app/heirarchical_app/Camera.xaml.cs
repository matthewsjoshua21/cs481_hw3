﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Media.Abstractions;
using Acr.UserDialogs;


namespace heirarchical_app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class MirrorPage : ContentPage
    {
        public MirrorPage()
        {
            InitializeComponent();
            //prompt user that page image will come from camera
            UserDialogs.Instance.Toast("Your picture will be the Image view", TimeSpan.MaxValue);
            //call camera function
            CameraDevice(this, EventArgs.Empty);


        }

        //camera function designed using plugin.media
        public async void CameraDevice(object sender, EventArgs e)
        {
            var media =  await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { });
            CameraImage.Source = ImageSource.FromStream(() => media.GetStream());
            

        }

        async void OnMirrorClick(object sender, EventArgs e)
        {
            App.last_page = "MirrorBtn";
            await Navigation.PushAsync(new MirrorPage());
        }

        async void OnMindClick(object sender, EventArgs e)
        {
            App.last_page = "MindBtn";
            await Navigation.PushAsync(new MindPage());
        }
        async void CalcBtn_OnClicked(object sender, EventArgs e)
        {
            App.last_page = "CalcBtn";
            await Navigation.PushAsync(new Calculator());
        }

        async void OnLastClick(object sender, EventArgs e)
        {
            if (App.last_page == "MirrorBtn")
            {
                App.last_page = "LastBtn";
                await Navigation.PushAsync(new MirrorPage());
            }
            else if (App.last_page == "MindBtn")
            {
                App.last_page = "LastBtn";
                await Navigation.PushAsync(new MindPage());
            }
            else if (App.last_page == "SuperPop")
            {
                App.last_page = "LastBtn";
                await Navigation.PopToRootAsync();
            }
            else if (App.last_page == "LastBtn")
            {
                App.last_page = "LastBtn";
                await DisplayAlert("Alert!", "This is not inception", "Ok");
            }
            else if (App.last_page == "CalcBtn")
            {
                App.last_page = "CalcBtn";
                await Navigation.PushAsync(new Calculator());
            }
            else
            {
                await DisplayAlert("System Error", "No valid navigation selected", "Ok");
            }
        }
        private void OnSuperCick(object sender, EventArgs e)
        {
            App.last_page = "SuperPop";
            Navigation.PopToRootAsync();
        }
        //Handle onAppearing and onDisappearing events. 
        private void MirrorPage_OnAppearing(object sender, EventArgs e)
        {
            base.OnAppearing();
            UserDialogs.Instance.Toast("Opening Camera...", TimeSpan.MinValue);
        }

        private void MirrorPage_OnDisappearing(object sender, EventArgs e)
        {
            base.OnDisappearing();
            UserDialogs.Instance.Toast("Leaving Camera...", TimeSpan.MinValue);
        }

      
    }

}