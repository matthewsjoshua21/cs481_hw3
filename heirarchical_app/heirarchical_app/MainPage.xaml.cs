﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace heirarchical_app
{

    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent(); 
          
        }

       
        //All on click methods are mirrored in each .cs file Global last-Page saves pages
        async void OnMirrorClick(object sender, EventArgs e)
        {
            App.last_page = "MirrorBtn";
            await Navigation.PushAsync(new MirrorPage());
        }

        async void OnMindClick(object sender, EventArgs e)
        {
            App.last_page = "MindBtn";
            await Navigation.PushAsync(new MindPage());
        }
        async void CalcBtn_OnClicked(object sender, EventArgs e)
        {
            App.last_page = "CalcBtn";
            await Navigation.PushAsync(new Calculator());
        }
        //If else block to handle last page button
        async void OnLastClick(object sender, EventArgs e)
        {
            if (App.last_page == "MirrorBtn")
            {
                App.last_page = "LastBtn";
                await Navigation.PushAsync(new MirrorPage());
            }
            else if (App.last_page == "MindBtn")
            {
                App.last_page = "LastBtn";
                await Navigation.PushAsync(new MindPage());
            }
            else if (App.last_page == "SuperPop")
            {
                App.last_page = "LastBtn";
                await Navigation.PopToRootAsync();
            }
            else if (App.last_page == "LastBtn")
            {
                App.last_page = "LastBtn";
                await DisplayAlert("Alert!", "This is not inception", "Ok");
            }
            else if (App.last_page == "CalcBtn")
            {
                App.last_page = "CalcBtn";
                await Navigation.PushAsync(new Calculator());
            }
            else
            {
                await DisplayAlert("System Error", "No valid navigation selected", "Ok");
            }
        }
        //Handle onAppearing and onDisappearing events. 
        private void MainPage_OnAppearing(object sender, EventArgs e)
        {
            base.OnAppearing();
            UserDialogs.Instance.Toast("Welcome to HW3...", TimeSpan.MaxValue);
        }

        private void MainPage_OnDisappearing(object sender, EventArgs e)
        {
            base.OnDisappearing();
            UserDialogs.Instance.Toast("Navigating pages...", TimeSpan.MinValue);
        }
    }
    
}
